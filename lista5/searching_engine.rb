require 'nokogiri'
require 'open-uri'

module SearchingEngine
	def initialize()
		@pages_statistic = {}
	end

	def showStatistic()
		@pages_statistic.each do |link,words|
			puts "================================================ "+link
			words.each do |word,number|
				puts "#{word} = #{number}"
			end
		end
	end

	#przegladanie strony na glebokosc depth i indeksowanie
	#slow ze stron
	def index(start_page, depth)
		pages_by_level = {}
		words = {}
		(1..depth).each do |i|
			pages_by_level[i] = []
			words[i] = []
		end

		uri = URI.parse(start_page)
		host_address = uri.host
		page = Nokogiri::HTML(open(start_page))

		pages_by_level[1].push start_page
		page.css('a').each do |node|
			begin
				uri = URI.parse(node.attributes['href'])
				if not node.attributes['href'].to_s.include? '#'
					if uri.host == host_address or uri.host == nil
						address = create_full_address(host_address,uri)
						if not array_includes_address?(pages_by_level[1],address)
							begin
								pages_by_level[1].push address
							rescue
							end
						end
					end
				end
			rescue
			end
		end

		if depth > 1
			(2..depth).each do |i|
				pages_by_level[i-1].each do |page_address|
					begin
						page = Nokogiri::HTML(open(page_address))
						page.css('a').each do |node|
							begin
								uri = URI.parse(node.attributes['href'])
								if not node.attributes['href'].to_s.include? '#'
									if uri.host == host_address or uri.host == nil
										address = create_full_address(host_address,uri)
										if not array_includes_address?(pages_by_level[i-1],address) and
											not array_includes_address?(pages_by_level[i],address)
											begin
												pages_by_level[i].push address
											rescue
											end
										end
									end
								end
							rescue
							end
						end
					rescue
					end
				end
			end
		end
		
		all_links = pages_by_level[1]

		if depth > 1
			(2..depth).each do |i|
				all_links = all_links.concat(pages_by_level[2])
			end
		end

		@pages_statistic = {}
		puts "first part done"
		all_links.each do |link|
			begin
				page = Nokogiri::HTML(open(link))
				page.search('script').each {|element| element.unlink}
				text = page.text
				words = {}
				text.scan(/[\w']+/) do |word|
					number = words[word]
					words[word] = number == nil ? 1 : number+1
				end
				@pages_statistic[link] = words
			rescue
			end
		end
	end

	#zwraca liste stron na ktorych wystepuja slowa pasujace do
	#wyrazenia
	def search(reg_exp)
		if @pages_statistic == {}
			puts "Brak stron do wyszukania!"
		else
			result = []
			@pages_statistic.each do |link,words|
				words.each do |word,number|
					if word =~ reg_exp
						result.push(link)
					end
				end
			end
			result
		end
	end


	private
		def array_includes_address?(array,address)
			includes = false
			array.each do |address_item|
				if address_item.to_s == address.to_s
					includes = true
				end
			end
			includes
		end

		def create_full_address(host,parsed_uri)
			if parsed_uri.host == nil
				if parsed_uri.to_s[0] != "/"
					address = "http://"+host+"/"+parsed_uri.to_s
				else
					address = "http://"+host+parsed_uri.to_s	
				end
			else
				return parsed_uri.to_s
			end		
		end

end