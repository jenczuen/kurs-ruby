require "rubygems"
require "active_record"

class CreateSongs <ActiveRecord::Migration
    def self.up
        create_table :songs do |t|
            t.string :title, :null => false
			t.integer :author_id
			t.integer :disc_id
            t.timestamps
        end
    end
    def self.down
        drop_table :songs
    end
end