require "rubygems"
require "active_record"

class CreateLogs <ActiveRecord::Migration
    def self.up
        create_table :logs do |t|
            t.integer :pid, :null => false
            t.string :msg
            t.timestamps
        end
    end
    def self.down
        drop_table :logs
    end
end