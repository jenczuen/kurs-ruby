require 'drb'
require 'rubygems'
require 'active_record'
require 'yaml'

class Log < ActiveRecord::Base
	attr_accessible :pid, :msg, :created_at

	def to_html
		html = "<div id=\"log_item\">\n"
		html += "\t<div id=\"pid\">"+pid.to_s+"</div>\n"
		html += "\t<div id=\"msg\">"+msg+"</div>\n"
		html += "\t<div id=\"time\">"+created_at.to_s+"</div>\n"
		html += "</div>\n"
	end
end


class LogServer
	def save(prg_id, msg)
		begin
			puts Time.new.to_s+" - odebralem log:"
			puts "pid: "+prg_id.to_s+", msg: "+msg
			Log.create(:pid => prg_id, :msg => msg)
		rescue
			puts "zly pid lub msg"
		end
	end

	def raport(from, to, prg_id, re)
		begin
			from_time = "#{from.year}-#{from.month}-#{from.day} #{from.hour}:#{from.min}:#{from.sec}"
			to_time = "#{to.year}-#{to.month}-#{to.day} #{to.hour}:#{to.min}:#{to.sec}"
			condition = "created_at > '#{from_time}'"
			condition += " AND created_at < '#{to_time}'"
			condition += " AND pid = '#{prg_id}'"

			logs_all = Log.where(condition)
#			puts condition
#			puts re
#			puts logs_all
			logs = []
			logs_all.each do |log|
				if(log.msg[re] != nil) 
					logs.push(log)
				end
			end

			html = "<html><body>\n"
			logs.each do |log|
				if log.respond_to?('to_html')
					html += log.to_html
				else
					html += "<div>" + log.to_s + "</div>\n"
				end
			end
			html += "</html></body>"

			return html
		rescue
			puts "zle argumenty"
		end		
	end

	def LogServer.run
		puts "connecting to the database...."
		@config = YAML::load(File.open('db/database.yml'))
		ActiveRecord::Base.establish_connection(@config["development"])		
		puts "connected!"

		puts "starting the server...."
		@@server = LogServer.new
		DRb.start_service('druby://localhost:9000', @@server)
		puts "The server is running at #{DRb.uri}"

		DRb.thread.join
	end

end


LogServer.run