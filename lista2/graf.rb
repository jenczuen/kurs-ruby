#!/usr/bin/ruby

class Graph
	attr_accessor :vertices
	def initialize(initial_vertices)
		@vertices = Hash.new {|hash,key| hash[key] = []}
		for x in initial_vertices 
			@vertices[x]
		end
	end

	def addConnection(a,b)
		if !(@vertices[a].include?(b))
			@vertices[a].push(b)
			@vertices[b].push(a)
		end
	end

	def v(a)
		vertices[a]
	end
	
	def to_s
		s = ""
		for x in @vertices
			s += x[0]+": "
			for y in @vertices[x[0]]
				s += y+" "
			end	
			s += "\n"
		end	
		s	
	end
end


def path(graf,start,stop)
	puts "Szukam w ponizszym grafie sciezki z "+start.to_s+" do "+stop.to_s
	puts graf.to_s

	wynik = []
	sasiedzi = graf.vertices[start]
	i = 0
	sasiedzi.each do |x|
		if x == stop
			wynik.push([start,stop])
		else
			lista = []
			wykorzystane = [start,x]
				p = pathStep(graf,x,stop,lista,wykorzystane,wynik)
			if(p == [])
				wynik.push([])
			else
				p.push(x)
				p.push(start)
				p.reverse!
				wynik.push(p)
			end
		end
		i+=1
	end
	wynik
end

def pathStep(graf,bierzacy,stop,lista,wykorzystane,wynik)
	sasiedzi = graf.vertices[bierzacy]
	sasiedzi.each do |x|
		#czy wykorzystane
		if !(wykorzystane.include?(x))
			#czy juz koniec
			if x == stop
				lista.push(x)
				return lista
			else
				#czy taka krawedz jak ta nie wystepuje juz w innej scieze
				#bierzac ----- x
				unikalne_krawedzie = true
				wynik.each do |w|
					#czy jakas sciezka juz zawiera te wierzcholki
					if (w.include?(bierzacy) and w.include?(x))
						#czy leza jeden po drugim (krawedz)
						if( w.index(bierzacy) == w.index(x)+1 or w.index(bierzacy) == w.index(x)-1)
							unikalne_krawedzie = false
						end	
					end
				end
				if unikalne_krawedzie
					wykorzystane.push(x)
					tmp = pathStep(graf,x,stop,lista,wykorzystane,wynik) 
					#czy ta droga nas do czegos doprowadzila
					if tmp == []
						return []
					else 
						lista = tmp.dup
						lista.push(x)
						return lista
					end
				end
			end
		end
	end
	return []
end

graf = Graph.new(["w1","w2","w3","w4","w5"])
graf.addConnection("w1","w2")
graf.addConnection("w1","w3")
graf.addConnection("w1","w5")
graf.addConnection("w3","w4")
graf.addConnection("w3","w5")
graf.addConnection("w4","w1")
graf.addConnection("w2","w5")
graf.addConnection("w5","w1")

p = path(graf,"w4","w5")
puts p.to_s



graf = Graph.new(["w1","w2","w3","w4","w5","w6","w7","w8","w9"])
graf.addConnection("w1","w2")
graf.addConnection("w1","w5")
graf.addConnection("w1","w6")
graf.addConnection("w1","w7")

graf.addConnection("w2","w9")
graf.addConnection("w2","w4")

graf.addConnection("w3","w9")
graf.addConnection("w3","w8")
graf.addConnection("w3","w6")
graf.addConnection("w3","w5")

graf.addConnection("w4","w5")

graf.addConnection("w7","w8")

p = path(graf,"w1","w5")
puts p.to_s


