#!/usr/bin/ruby
#============================= testy
require "test/unit"
require "graf.rb"

class TestGraph < Test::Unit::TestCase
	def testEmpty
		graf = Graph.new([])
		assert_equal({},graf.vertices)
	end

	def testCreation
		graf = Graph.new(["w1","w2","w3","w4","w5"])
		assert_equal({"w1"=>[],"w2"=>[],"w3"=>[],"w4"=>[],"w5"=>[]}, graf.vertices)
	end	

	def testPresentation
		graf = Graph.new(["w1","w2","w3","w4","w5"])
		assert_equal("jak ma wygladac reprezentacja grafu",graf.to_s)
	end

	def testConnections
		graf = Graph.new(["w1","w2","w3","w4","w5"])
		graf.addConnection("w1","w2")
		graf.addConnection("w1","w3")
		graf.addConnection("w1","w5")
		graf.addConnection("w3","w4")
		graf.addConnection("w3","w5")
		graf.addConnection("w4","w1")
		graf.addConnection("w2","w5")
		graf.addConnection("w5","w1")
		assert_equal({"w1"=>["w2","w3","w4","w5"],
					  "w2"=>["w1","w5"],
					  "w3"=>["w1","w4","w5"],
					  "w4"=>["w1","w3"],
					  "w5"=>["w1","w3"]},
			graf.vertices)
	end
end