require_relative 'searching_engine.rb'

class SearchingEngineTester
	include SearchingEngine
end

engine = SearchingEngineTester.new()

engine.index("http://www.wroc.gm-auto.pl/", 2)
engine.showStatistic()
puts engine.search(/lakier/)