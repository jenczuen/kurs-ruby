require 'nokogiri'
require 'open-uri'

module SearchingEngine
	def initialize()
		@pages_statistic = {}
	end

	def showStatistic()
		@pages_statistic.each do |link,words|
			puts "================================================ "+link
			words.each do |word,number|
				puts "#{word} = #{number}"
			end
		end
	end

	#przegladanie strony na glebokosc depth i indeksowanie
	#slow ze stron
	def index(start_page, depth)
		visited_links = []
		not_visited_links = []
		words = {}

		not_visited_links.push start_page
		get_all_links_from_a_page(not_visited_links,visited_links,start_page)

		(1..depth).each do
			threads = []
			links_to_visit = not_visited_links.dup
			links_to_visit.each do |link|
				threads.push Thread.new{
					get_all_links_from_a_page(not_visited_links,visited_links,link)
				}
			end
			threads.each {|t| t.join}
		end

		threads = []
		@pages_statistic = {}
		visited_links.each do |link|
			threads.push Thread.new{ get_pages_statistic(link) }
		end
		not_visited_links.each do |link|
			threads.push Thread.new{ get_pages_statistic(link) }
		end

		threads.each {|t| t.join}
	end

	#zwraca liste stron na ktorych wystepuja slowa pasujace do
	#wyrazenia
	def search(reg_exp)
		if @pages_statistic == {}
			puts "Brak stron do wyszukania!"
		else
			result = []
			threads = []
			@pages_statistic.each do |link,words|
				threads.push Thread.new{
					search_regex_in_a_hash_with_words(result,link,words,reg_exp)
				}
			end
			threads.each {|t| t.join}
			result
		end
	end


	private

		def print_all_links
			if @pages_statistic == {}
				puts "Brak stron do wyszukania!"
			else			
				@pages_statistic.each do |link,words|
					puts "got link: "+link
				end
			end
		end

		def search_regex_in_a_hash_with_words(result,link,words,reg_exp)
			words.each do |word,number|
				if word =~ reg_exp
					result.push(link)
					return
				end
			end
		end


		def get_all_links_from_a_page(not_visited_links,visited_links,start_page_address)
			begin
				host_address = (URI.parse(start_page_address)).host
				page = Nokogiri::HTML(open(start_page_address))
				page.css('a').each do |node|
					begin
						uri = URI.parse(node.attributes['href'])
						address = create_full_address_within_the_host(host_address,uri)
						if address != nil							
							if (not array_includes_address?(visited_links,address)) and
							   (not array_includes_address?(not_visited_links,address))
								
								not_visited_links.push address
							end
						end
					rescue
					end
				end
				not_visited_links.delete start_page_address
				visited_links.push start_page_address
			rescue
#				puts "failed to open: "+start_page_address
			end
		end

		def get_pages_statistic(link)
			begin
				page = Nokogiri::HTML(open(link))
				page.search('script').each {|element| element.unlink}
				text = page.text
				words = {}
				text.scan(/[\w']+/) do |word|
					number = words[word]
					words[word] = number == nil ? 1 : number+1
				end
				@pages_statistic[link] = words
			rescue
			end
		end

		def array_includes_address?(array,address)
			includes = false
			array.each do |address_item|
				if address_item.to_s == address.to_s
					includes = true
				end
			end
			includes
		end

		def create_full_address_within_the_host(host,parsed_uri)
			address = parsed_uri.to_s
			if (not address.include? '#') and 
			   (not address.include? 'javascript:') and
			   (not address.include? 'mailto:') and
			   (not address.include? '.jpg') and
			   (not address.include? '.JPG') and
			   (not address.include? '.png') and
			   (not address.include? '.gif')
				
				if parsed_uri.host == nil
					if parsed_uri.to_s[0] != "/"		
						return "http://"+host+"/"+address
					else
						return "http://"+host+address
					end
				else
					if parsed_uri.host == host
						return address
					else
						return nil
					end
				end		
			else 
				return nil
			end
		end

end