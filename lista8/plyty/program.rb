#!/usr/bin/ruby
require 'rubygems'
require 'yaml'
require_relative "modele.rb"

class Interface

	def start
		@config = YAML::load(File.open('db/database.yml'))
		ActiveRecord::Base.establish_connection(@config["development"])

		@komenda = ""
		@info_menu_glowne = "1 - Wyswietl wszystkie plyty\n"
		@info_menu_glowne += "2 - Dodaj plyte\n"
		@info_menu_glowne += "3 - Usun plyte\n"
		@info_menu_glowne += "4 - wypozyczenie plyty\n"
		@info_menu_glowne += "5 - Oddanie plyty\n"
		@info_menu_glowne += "6 - Wyjscie"

		self.menu_glowne
	end

	def menu_glowne
		while @komenda != "6"
			puts @info_menu_glowne
			print "komenda: "
			@komenda = gets.chomp
			case @komenda
				when "1"
					self.wyswietl_plyty
				when "2"
					self.dodaj_plyte
				when "3"
					self.usun_plyte
				when "4"
					self.wypozycz_plyte
				when "5"
					self.oddaj_plyte
				when "6"
				else
					puts "nieznane polecenie"
			end
		end
	end

	def wyswietl_plyty
		authors = Author.all()
		puts
		authors.each do |author|
			plyty = author.discs
			plyty.each do |plyta|
				puts "\tTytul: "+plyta.name
				puts "\tAutor: "+plyta.author.name
				piosenki = plyta.songs
				track_nr = 1
				piosenki.each do |piosenka|
					puts "\t\t"+track_nr.to_s+". "+piosenka.title
					track_nr += 1
				end
				print "\tPlyta wypozyczona: "
				puts plyta.borrowed==1 ? "tak" : "nie"
				puts
			end
		end
	end

	def dodaj_plyte
		puts
		print "\t Podaj nazwe plyty: "
		name = gets.chomp
		plyta = Disc.create(:name => name)

		print "\t Podaj autora: "
		name = gets.chomp
		autor = Author.where(:name => name).first
		if autor == nil
			autor = Author.create(:name => name)
		end
		plyta.author = autor
		plyta.save

		title = ""
		while title != "koniec"
			print "\t Tytul piosenki (koniec jezeli chcesz zakonczyc): "
			title = gets.chomp
			if title != "koniec"
				piosenka = Song.create(:title => title)
				piosenka.disc = plyta
				piosenka.author = autor
				piosenka.save
			end
		end
		puts
	end

	def usun_plyte
		puts
		plyty = Disc.all()
		plyty.each do |plyta|
			puts "\t"+plyta.name+", id = "+plyta.id.to_s
		end
		id = ""
		puts
		while id != "koniec"
			print "\tId plyty (lub koniec jezeli chcesz zakonczyc): "
			id = gets.chomp
			if id != "koniec"
				plyta = Disc.find(Integer(id))
				nazwa = plyta.name
				autor = plyta.author
				if(autor.discs.length == 1)
					autor.destroy
				end
				plyta.destroy				
				puts "\tUsunieta plyte \""+nazwa+"\""
			end
		end
		puts
	end

	def wypozycz_plyte
		puts
		plyty = Disc.where("borrowed = '0'")
		if plyty != []
			puts "\tNiewypozyczone plyty"
			plyty.each do |plyta|
				puts "\t"+plyta.name+", id = "+plyta.id.to_s
			end
			id = ""
			puts
			print "\tId plyty do wypozyczenia: "
			id = gets.chomp
			plyta = Disc.find(Integer(id))
			nazwa = plyta.name
			plyta.borrowed = 1;
			plyta.save;
			puts "\twypozyczono plyte \""+nazwa+"\""
			puts
		else 
			print "\tWszystkie plyty wypozyczone\n\n"
		end
	end

	def oddaj_plyte
		puts
		plyty = Disc.where("borrowed = '1'")
		if plyty != []
			puts "\tWypozyczone plyty"
			plyty.each do |plyta|
				puts "\t"+plyta.name+", id = "+plyta.id.to_s
			end
			id = ""
			puts
			print "\tId oddawanej plyty: "
			id = gets.chomp
			plyta = Disc.find(Integer(id))
			nazwa = plyta.name
			plyta.borrowed = 0;
			plyta.save;
			puts "\tOddano plyte \""+nazwa+"\""
			puts
		else
			print "\tNic nie wypozyczono!\n\n"
		end
	end

end

interfejs = Interface.new()
interfejs.start()