require_relative "../modele.rb"
require "test/unit"
 
class TestAuthors < Test::Unit::TestCase

	def connect_to_the_test_database
		@config = YAML::load(File.open('../db/database.yml'))
		ActiveRecord::Base.establish_connection(@config["test"])
	end

	def test_creating_authors
		connect_to_the_test_database()
		length = Author.all().length
		author = Author.create(:name => "test author")
		
		assert_equal(length+1, Author.all().length)
		assert_equal(author, Author.last())
		assert_equal(author.name, "test author")
	end

	def test_finding_authors
		connect_to_the_test_database()
		Author.create(:name => "1")
		author = Author.create(:name => "2")
		Author.create(:name => "3")
		assert_equal(author, Author.where("name = '2'").last())
		assert_equal(author, Author.find(author.id))
	end

	def test_removing_Author
		authors = Author.all()
		authors.each do |author|
			length = Author.all().length
			author.delete
			assert_equal(length-1, Author.all().length)
		end
	end 
 
end