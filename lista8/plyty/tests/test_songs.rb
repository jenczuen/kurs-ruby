require_relative "../modele.rb"
require "test/unit"
 
class TestSongs < Test::Unit::TestCase
 
	def connect_to_the_test_database
		@config = YAML::load(File.open('../db/database.yml'))
		ActiveRecord::Base.establish_connection(@config["test"])
	end

	def test_creating_songs
		connect_to_the_test_database()
		length = Song.all().length
		song = Song.create(:title => "test song")
		
		assert_equal(length+1, Song.all().length)
		assert_equal(song, Song.last())
		assert_equal(song.title, "test song")
	end

	def test_finding_songs
		connect_to_the_test_database()
		Song.create(:title => "1")
		song = Song.create(:title => "2")
		Song.create(:title => "3")
		assert_equal(song, Song.where("title = '2'").last())
		assert_equal(song, Song.find(song.id))
	end

	def test_removing_Song
		songs = Song.all()
		songs.each do |song|
			length = Song.all().length
			song.delete
			assert_equal(length-1, Song.all().length)
		end
	end 
 
end