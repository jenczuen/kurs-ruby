require_relative "../modele.rb"
require "test/unit"
 
class TestDiscs < Test::Unit::TestCase
 
	def connect_to_the_test_database
		@config = YAML::load(File.open('../db/database.yml'))
		ActiveRecord::Base.establish_connection(@config["test"])
	end

	def test_creating_discs
		connect_to_the_test_database()
		length = Disc.all().length
		disc = Disc.create(:name => "test disc")
		
		assert_equal(length+1, Disc.all().length)
		assert_equal(disc, Disc.last())
		assert_equal(disc.name, "test disc")
	end

	def test_finding_discs
		connect_to_the_test_database()
		Disc.create(:name => "1")
		disc = Disc.create(:name => "2")
		Disc.create(:name => "3")
		assert_equal(disc, Disc.where("name = '2'").last())
		assert_equal(disc, Disc.find(disc.id))
	end

	def test_borrowing_disc
		connect_to_the_test_database()
		disc = Disc.create(:name => "test borrow")
		assert_equal(disc.borrowed, 0)
		disc.borrowed = 1
		disc.save
		disc.reload
		assert_equal(disc.borrowed, 1)
	end

	def test_removing_disc
		discs = Disc.all()
		discs.each do |disc|
			length = Disc.all().length
			disc.destroy
			assert_equal(length-1, Disc.all().length)
		end
	end 
end