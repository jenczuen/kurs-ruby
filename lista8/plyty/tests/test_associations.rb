require_relative "../modele.rb"
require "test/unit"
 
class TestAssociations < Test::Unit::TestCase

	def connect_to_the_test_database
		@config = YAML::load(File.open('../db/database.yml'))
		ActiveRecord::Base.establish_connection(@config["test"])
	end

	def test_disc_has_many_songs_dependet_destroy
		connect_to_the_test_database()

		disc = Disc.create(:name => "Plyta")
		track1 = Song.create(:title => "Track1")
		track1.disc = disc
		track1.save
		track2 = Song.create(:title => "Track2")
		track2.disc = disc
		track2.save

		songs = disc.songs
		assert_equal(true, songs.include?(track1))
		assert_equal(true, songs.include?(track2))
		assert_equal(2, songs.length)

		disc.destroy
		songs = Song.all()
		assert_equal(false, songs.include?(track1))
		assert_equal(false, songs.include?(track2))
	end

	def test_author_has_many_discs
		connect_to_the_test_database()

		author = Author.create(:name => "autor")
		disc1 = Disc.create(:name => "disc1")
		disc1.author = author
		disc1.save
		disc2 = Disc.create(:name => "disc2")
		disc2.author = author
		disc2.save

		discs = author.discs
		assert_equal(true, discs.include?(disc1))
		assert_equal(true, discs.include?(disc2))
		assert_equal(2, discs.length)

		author.destroy
		discs = Disc.all()
		assert_equal(true, discs.include?(disc1))
		assert_equal(true, discs.include?(disc2))

		discs.each {|d| d.destroy}
	end

	def test_author_has_many_songs_through_discs
		connect_to_the_test_database()

		author = Author.create(:name => "autor")
		disc = Disc.create(:name => "disc")
		disc.author = author
		disc.save
		track1 = Song.create(:title => "Track1")
		track1.disc = disc
		track1.save
		track2 = Song.create(:title => "Track2")
		track2.disc = disc
		track2.save

		songs = author.songs
		assert_equal(true, songs.include?(track1))
		assert_equal(true, songs.include?(track2))
		assert_equal(2, songs.length)

		author.destroy
		songs = Song.all()
		assert_equal(true, Disc.all().include?(disc))
		assert_equal(true, songs.include?(track1))
		assert_equal(true, songs.include?(track2))

		disc.destroy
		songs = Song.all()		
		assert_equal(false, songs.include?(track1))
		assert_equal(false, songs.include?(track2))
	end
 
end