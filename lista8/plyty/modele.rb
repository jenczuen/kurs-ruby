require 'active_record'

class Disc < ActiveRecord::Base
	attr_accessible :name, :borrowed

	has_many :songs, :dependent => :destroy
	belongs_to :author
end

class Song < ActiveRecord::Base
	attr_accessible :title

	belongs_to :disc
	belongs_to :author
end

class Author < ActiveRecord::Base
	attr_accessible :name

	has_many :discs
	has_many :songs, :through => :discs
end
