require "rubygems"
require "active_record"

class CreateDiscs <ActiveRecord::Migration
	def self.up
		create_table :discs do |t|
			t.string :name, :null => false
			t.integer :author_id
			t.integer :borrowed, :default => 0
			t.timestamps   
		end
	end
	def self.down
		drop_table :discs
	end
end