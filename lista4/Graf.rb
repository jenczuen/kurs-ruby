#!/usr/bin/ruby
require_relative 'Debugger.rb'

class Graf
	include Debugger
	attr_reader :vertices
	def initialize(initial_vertices)
		@vertices = Hash.new {|hash,key| hash[key] = []}
		for x in initial_vertices 
			@vertices[x]
		end
	end

	def addConnection(a,b)
		if !(@vertices[a].include?(b))
			@vertices[a].push(b)
			@vertices[b].push(a)
			@vertices[a].sort!
			@vertices[b].sort!			
		end
	end
	
	def to_s
		s = ""
		for x in @vertices
			s += x[0]+": "
			for y in @vertices[x[0]]
				s += y+" "
			end	
			s += "\n"
		end	
		s	
	end

	def path(start,stop)
		puts "Szukam w ponizszym grafie sciezki z "+start.to_s+" do "+stop.to_s
		puts self.to_s

		wynik = []
		sasiedzi = self.vertices[start]
		i = 0
		sasiedzi.each do |x|
			if x == stop
				wynik.push([start,stop])
			else
				lista = []
				wykorzystane = [start,x]
					p = pathStep(x,stop,lista,wykorzystane,wynik)
				if(p == [])
					wynik.push([])
				else
					p.push(x)
					p.push(start)
					p.reverse!
					wynik.push(p)
				end
			end
			i+=1
		end
		wynik
	end

	private 
	def pathStep(bierzacy,stop,lista,wykorzystane,wynik)
		sasiedzi = self.vertices[bierzacy]
		sasiedzi.each do |x|
			#czy wykorzystane
			if !(wykorzystane.include?(x))
				#czy juz koniec
				if x == stop
					lista.push(x)
					return lista
				else
					#czy taka krawedz jak ta nie wystepuje juz w innej scieze
					#bierzac ----- x
					unikalne_krawedzie = true
					wynik.each do |w|
						#czy jakas sciezka juz zawiera te wierzcholki
						if (w.include?(bierzacy) and w.include?(x))
							#czy leza jeden po drugim (krawedz)
							if( w.index(bierzacy) == w.index(x)+1 or w.index(bierzacy) == w.index(x)-1)
								unikalne_krawedzie = false
							end	
						end
					end
					if unikalne_krawedzie
						wykorzystane.push(x)
						tmp = pathStep(x,stop,lista,wykorzystane,wynik) 
						#czy ta droga nas do czegos doprowadzila
						if tmp == []
							return []
						else 
							lista = tmp.dup
							lista.push(x)
							return lista
						end
					end
				end
			end
		end
		return []
	end

	#--------------------- test methods
	private
	def test_to_string
		graf = Graf.new(["w1","w2","w3","w4","w5"])
		graf.addConnection("w1","w2")
		graf.addConnection("w1","w3")
		graf.addConnection("w1","w5")
		graf.addConnection("w2","w5")
		graf.addConnection("w3","w4")
		graf.addConnection("w3","w5")
		graf.addConnection("w4","w1")
		graf.addConnection("w5","w1")

		s  = "w1: w2 w3 w4 w5 \n"
		s += "w2: w1 w5 \n"
		s += "w3: w1 w4 w5 \n"
		s += "w4: w1 w3 \n"
		s += "w5: w1 w2 w3 \n"

		return graf.to_s == s ? "Sukces!" : "Porazka!"

	end
end