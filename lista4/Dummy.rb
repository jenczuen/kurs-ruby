require_relative 'Debugger.rb'

class Dummy
	include Debugger
	attr_reader :a,:b
	def initialize(a,b)
		@a = a
		@b = b
	end

	def change(new_a,new_b)
		@a = a
		@b = b		
	end	

	private
	def test_change
		d = Dummy.new(1,2)
		if (d.a == 1) && (d.b == 2) 
			return "Sukces"
		else 
			return "Porazka"
		end
	end

	def test_wrong_arguments
		d = Dummy.new()
	end
end