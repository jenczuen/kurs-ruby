module Debugger
	def snapshot
		puts "Stan obiektu klasy #{self.class}"
		for iv in self.instance_variables
			puts "#{iv} = #{self.instance_variable_get(iv)}"
		end
	end

	def check
		test_methods = []
		for m in self.private_methods
			if m.to_s.include? "test_"
				test_methods.push(m)
			end
		end
		puts
		for t in test_methods
			puts "Wynik #{t.to_s}:"
			begin
				puts "\t"+(self.send t);
				puts
			rescue 
				puts "\tWyjatek w #{t.to_s}: #{$!}"
				puts
			end
		end
	end

end