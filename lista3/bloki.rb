
def calka(a,b,&f)
	if block_given?
		n=10000		
		c = 0
		(a*n..b*n).each {|x| c += (f.call x.to_f/n.to_f)/n.to_f }
		c
	else 
		puts "Nie dostalem funkcji!"
	end
end


def wykres(a,b,&f)
	if block_given?
		n = 20
		y_max = 1
#		y_max = 2.5		
		s = ""
		(0..2*y_max*n).each {|y| (a*n..b*n).each {|x| (y_max*n-y == ((f.call x.to_f/n.to_f)*n.to_f).to_i) ? s+="*" : s+=" " } and s+="\n" }
		puts s
	else 
		puts "Nie dostalem funkcji!"
	end
end


c = calka 0, 3.14 do |x| 
#	(x)*(x)
#	Math.log(x)
	Math.sin(x)
end
puts c

wykres 0, 6.30 do |x| 
#	(x)*(x)*(x)
	Math.sin(x)
#	Math.log(x)
end

