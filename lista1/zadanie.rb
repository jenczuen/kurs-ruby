#!/usr/bin/ruby


require 'date'

class List1
	#============================================= 1
	def self.f1(rok,miesiac,dzien)
		arg = Date.new(rok,miesiac,dzien)
		baza = Date.new(2010,12,31)
		days = 0
		(baza-arg).ceil
	end

	#============================================= 2
	def self.f2(numbers)
		if numbers.is_a?(Array)
			numbers.inject{|sum,x| sum + x }
		else 
			:undefined
		end
	end

	#============================================= 3
	def self.f3(d1,d2)
		date1 = Date.new(d1[2],d1[1],d1[0]) 
		date2 = Date.new(d2[2],d2[1],d2[0]) 	
		days = (date1-date2).ceil
		days < 0 ? -days : days
	end

	#============================================= 4
	def self.f4(number)
		words = [
			"zero",
			"jeden",
			"dwa",
			"trzy",
			"cztery",
			"piec",
			"szesc",
			"siedem",
			"osiem",
			"dziewiec",
		]
		numbers = number.to_s.split("")
		output = []
		for n in numbers 
			output.push words[n.to_i]
		end
		output.join(" ")
	end

	#============================================= 5
	def self.f5(numbers)
		words = {
			:zero => "0",
			:jeden => "1",
			:dwa => "2",
			:trzy => "3",
			:cztery => "4",
			:piec => "5",
			:szesc => "6",
			:siedem => "7",
			:osiem => "8",
			:dziewiec => "9"
		}
		output = []
		for n in numbers 
			output.push words[n.to_sym]
		end
		output.join.to_s
	end

end

#============================= testy
require "test/unit"

class TestFunction1 < Test::Unit::TestCase
	def testDates
		assert_equal 88, List1.f1(2010,10,4)
		assert_equal 1, List1.f1(2010,12,30)
	end
end

class TestFunction2 < Test::Unit::TestCase
	#argument - lista liczb
	#wynik - suma liczb z listy
	def testAdding
		assert_equal 55, List1.f2((1..10).to_a)
		assert_equal :undefined, List1.f2("cos")
	end
end

class TestFunction3 < Test::Unit::TestCase
	#argument - 2 listy 3 liczb (daty)
	#wynik - ile dni je dzieli
	def testDates
		assert_equal 275, List1.f3([1,1,2002],[1,4,2001])
		assert_equal 275, List1.f3([1,4,2001],[1,1,2002])
	end
end

class TestFunction4 < Test::Unit::TestCase
	#argument - liczba
	#wynik - liczba slownie
	def testNumber
		assert_equal "jeden zero dwa trzy jeden cztery trzy", List1.f4(1023143)
	end
end

class TestFunction5 < Test::Unit::TestCase
	#argument - liczba slownie
	#wynik - liczba
	def testNumber
		assert_equal "1023143", 
			List1.f5("jeden zero dwa trzy jeden cztery trzy".split(" "))
	end
end

